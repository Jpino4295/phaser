// MAIN
var formatAMPM, state_data, state_hour, state_main;

state_main = function() {
  return {
    preload: function() {
      game.counter = 0;
      game.load.spritesheet(game.digi, `assets/spritesheet/${game.digi}.png`, 32, 32, 2);
      game.load.image('bkg', 'img/plastic.gif');
      game.load.spritesheet('button', 'img/buttons.png', 32, 32);
    },
    create: function() {
      var back, sprite;
      back = game.add.sprite(0, 0, 'bkg');
      sprite = game.add.sprite(40, 100, game.digi);
      sprite.animations.add('bounce');
      sprite.animations.play('bounce', 2, true);
      game.set_buttons();
    },
    update: function() {
      var counter;
      if (counter >= 8) {
        $(game.a_menu).css('opacity', '0.5');
        counter = 0;
      }
    },
    decisor: function(s_function) {
      game.state.start(s_function);
    }
  };
};

// HORA
formatAMPM = function(date) {
  var ampm, hours, minutes, o_time;
  hours = date.getHours();
  minutes = date.getMinutes();
  ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12;
  // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  o_time = {
    s_hour: hours + ':' + minutes,
    s_ampm: ampm
  };
  return o_time;
};

state_hour = function() {
  var date, o_style, o_style_ampm, s_date;
  date = new Date;
  s_date = formatAMPM(date);
  o_style = {
    font: '40px clock',
    fill: '#000',
    align: 'center'
  };
  o_style_ampm = {
    font: '40px clock',
    fill: '#000'
  };
  return {
    preload: function() {
      game.load.image('bkg', 'img/plastic.gif');
      game.load.spritesheet('button', 'img/buttons.png', 32, 32);
    },
    create: function() {
      var back;
      back = game.add.sprite(0, 0, 'bkg');
      game.add.text(55, 60, s_date.s_hour, o_style);
      game.add.text(65, 90, s_date.s_ampm.toUpperCase(), o_style_ampm);
      game.set_buttons();
      game.btn_a.inputEnabled = false;
      game.btn_b.inputEnabled = false;
    },
    update: function() {}
  };
};

// DATOS
state_data = function() {
  return {
    preload: function() {
      game.load.image('bkg', 'img/plastic.gif');
      game.load.spritesheet('button', 'img/buttons.png', 32, 32);
    },
    create: function() {
      var back;
      back = game.add.sprite(0, 0, 'bkg');
      game.set_buttons();
      //Sobreescribir la funcion de los botones
      game.btn_a = game.add.button(350, 20, 'button', function() {
        return alert("BTN_A");
      }, 1, 0, 2);
      game.btn_b = game.add.button(350, 85, 'button', function() {
        return alert("BTN_B");
      }, 1, 0, 2);
    },
    update: function() {},
    decisor: function(s_function) {}
  };
};
