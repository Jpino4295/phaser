// Cordova
var app, game;

app = {
  initialize: function() {
    document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
  },
  onDeviceReady: function() {
    this.receivedEvent('deviceready');
  },
  receivedEvent: function(id) {
    var listeningElement, parentElement, receivedElement;
    parentElement = document.getElementById(id);
    listeningElement = parentElement.querySelector('.listening');
    receivedElement = parentElement.querySelector('.received');
    listeningElement.setAttribute('style', 'display:none;');
    receivedElement.setAttribute('style', 'display:block;');
    console.log('Received Event: ' + id);
  }
};

app.initialize();

// Instanciamos un objeto Game de phasert
game = new Phaser.Game(300, 200, Phaser.AUTO, 'phaser', this, true);

// Añadimos states
game.state.add('main', state_main);

game.state.add('hour', state_hour);

game.state.add('data', state_data);

// menú de opciones
game.a_menu = document.getElementsByTagName('li');

// Primer bicho siempre el mismo
game.digi = "botamon";

//Imagen de fondo

// Función para crear u añadir funcionalidad a los botones
game.set_buttons = function() {
  // Selector del menú
  game.btn_a = game.add.button(200, 35, 'button', function() {
    $(game.a_menu).css('opacity', '0.2');
    //Si el contador es mayor que 8, lo volvemos a poner a 0
    if (game.counter < 7) {
      //Poner el icono que toca negro
      $('#' + game.a_menu[game.counter + 1].id).css('opacity', '1');
      // console.log(game.counter);
      return game.counter += 1;
    } else {
      return game.counter = 0;
    }
  // down, over, out
  }, this, 0, 0, 1);
  // Botón de aceptar
  game.btn_b = game.add.button(200, 80, 'button', function() {
    var s_function;
    // Llamamos al state que se llama como el id del elemento
    s_function = $('#' + game.a_menu[game.counter].id).attr('id');
    game.state.getCurrentState()['decisor'](s_function);
  }, this, 0, 0, 1);
  // Botón de cancelar
  game.btn_c = game.add.button(200, 125, 'button', function() {
    game.state.start('main');
    game.counter = 0;
    $(game.a_menu).css('opacity', '0.2');
    game.set_buttons();
  }, this, 0, 0, 1);
};

game.state.start('main');
