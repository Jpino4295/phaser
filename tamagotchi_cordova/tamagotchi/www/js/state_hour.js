var formatAMPM, state_hour;

formatAMPM = function(date) {
  var ampm, hours, minutes, o_time;
  hours = date.getHours();
  minutes = date.getMinutes();
  ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12;
  // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes;
  o_time = {
    s_hour: hours + ':' + minutes,
    s_ampm: ampm
  };
  return o_time;
};

state_hour = function() {
  var date, o_style, o_style_ampm, s_date;
  date = new Date;
  s_date = formatAMPM(date);
  o_style = {
    font: '65px clock',
    fill: '#000',
    align: 'center'
  };
  o_style_ampm = {
    font: '65px clock',
    fill: '#000'
  };
  return {
    preload: function() {
      game.set_buttons();
      game.btn_a.inputEnabled = false;
      game.btn_b.inputEnabled = false;
    },
    create: function() {
      game.add.text(100, 0, s_date.s_hour, o_style);
      game.add.text(110, 100, s_date.s_ampm.toUpperCase(), o_style_ampm);
    },
    update: function() {},
    decisor: function() {}
  };
};
