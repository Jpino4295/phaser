# MAIN
state_main = () ->
  preload: ->
    game.counter = 0
    game.load.spritesheet(game.digi, "assets/spritesheet/#{game.digi}.png", 32, 32, 2)
    game.load.image('bkg', 'img/plastic.gif')
    game.load.spritesheet('button', 'img/buttons.png', 32, 32)

    return
  create: ->
    back = game.add.sprite(0, 0, 'bkg')
    sprite = game.add.sprite(40, 100, game.digi)
    sprite.animations.add('bounce')
    sprite.animations.play('bounce', 2, true)
    game.set_buttons()

    return
  update: ->
    if counter >= 8
      $(game.a_menu).css 'opacity', '0.5'
      counter = 0
    return
  decisor: (s_function) ->
    game.state.start s_function
    return



# HORA

formatAMPM = (date) ->
  hours = date.getHours()
  minutes = date.getMinutes()
  ampm = if hours >= 12 then 'pm' else 'am'
  hours = hours % 12
  hours = if hours then hours else 12
  # the hour '0' should be '12'
  minutes = if minutes < 10 then '0' + minutes else minutes
  o_time =
    s_hour: hours + ':' + minutes
    s_ampm: ampm
  o_time

state_hour = () ->

  date = new Date
  s_date = formatAMPM(date)
  o_style =
    font: '40px clock'
    fill: '#000'
    align: 'center'
  o_style_ampm =
    font: '40px clock'
    fill: '#000'

  preload: ->
    game.load.image('bkg', 'img/plastic.gif')
    game.load.spritesheet('button', 'img/buttons.png', 32, 32)
    return

  create: ->
    back = game.add.sprite(0, 0, 'bkg')
    game.add.text(55, 60, s_date.s_hour, o_style)
    game.add.text(65, 90, s_date.s_ampm.toUpperCase(), o_style_ampm)
    game.set_buttons()
    game.btn_a.inputEnabled = false
    game.btn_b.inputEnabled = false
    return

  update: ->



# DATOS
state_data = () ->
  preload: ->
    game.load.image('bkg', 'img/plastic.gif')
    game.load.spritesheet('button', 'img/buttons.png', 32, 32)



    return
  create: ->
    back = game.add.sprite(0, 0, 'bkg')
    game.set_buttons()
    #Sobreescribir la funcion de los botones
    game.btn_a = game.add.button(350, 20, 'button', () ->
      alert "BTN_A"
    , 1, 0, 2)

    game.btn_b = game.add.button(350, 85, 'button', () ->
      alert "BTN_B"
    , 1, 0, 2)
    return
  update: ->

    return
  decisor: (s_function) ->

    return

# Comida
state_food = () ->
  preload: ->
    game.load.image('bkg', 'img/plastic.gif')
    game.load.spritesheet('button', 'img/buttons.png'game.load.sprite, 32, 32)
    # cargar las imágenes de la comida como selectores


    return
  create: ->
    back = game.add.sprite(0, 0, 'bkg')
    game.set_buttons()
    #Sobreescribir la funcion de los botones
    game.btn_a = game.add.button(350, 20, 'button', () ->
      alert "BTN_A"
    , 1, 0, 2)

    game.btn_b = game.add.button(350, 85, 'button', () ->
      alert "BTN_B"
    , 1, 0, 2)
    return
  update: ->

    return
  decisor: (s_function) ->

    return


# Train
# No puede hacerlo de bebe (Primer dia)
state_train = () ->
  preload: ->
    game.load.image('bkg', 'img/plastic.gif')
    game.load.spritesheet('button', 'img/buttons.png', 32, 32)
    #Cargamos el spritesheet del bicho. Necesitamos 2, uno arriba y otro abajo.
    game.load.spritesheet(game.digi, "assets/spritesheet/#{game.digi}.png", 32, 32, 2)


    return
  create: ->
    back = game.add.sprite(0, 0, 'bkg')
    game.set_buttons()
    #Sobreescribir la funcion de los botones
    game.btn_a = game.add.button(350, 20, 'button', () ->
      alert "Golpear arriba"
      create_sprites()
    , 1, 0, 2)

    game.btn_b = game.add.button(350, 85, 'button', () ->
      create_sprites('down')
      alert "Golpear abajo"
    , 1, 0, 2)

    create_sprites = (where = 'up') ->

      # Crear dos objetos, uno el que controla el usuario y otro el que controla la maquina
      # El objeto de la maquina está invertido
      player_sprite = game.add.sprite(40, 100, game.digi)
      #ai_sprite = game.add.sprite(40, 100, game.digi)

      if where is 'up'
        # Crear el sprite del ataque por arriba
      else if where is 'down'
        # Crear el sprite del ataque por abajo

      #Crear sprites del ataque para la ia
      create_sprites(ia_decisor())


    ia_decisor = () ->
      rand = Math.random()
      if rand < 0.5
        return 'up'
      else return 'down'

    return
  update: ->

    return
  decisor: (s_function) ->

    return


# Lucha
state_fight = () ->
  preload: ->
    game.load.image('bkg', 'img/plastic.gif')
    game.load.spritesheet('button', 'img/buttons.png', 32, 32)



    return
  create: ->
    back = game.add.sprite(0, 0, 'bkg')
    game.set_buttons()
    #Sobreescribir la funcion de los botones
    game.btn_a = game.add.button(350, 20, 'button', () ->
      alert "BTN_A"
    , 1, 0, 2)

    game.btn_b = game.add.button(350, 85, 'button', () ->
      alert "BTN_B"
    , 1, 0, 2)
    return
  update: ->

    return
  decisor: (s_function) ->

    return


# WC
## Crea una animación que limpia el bicho si tiene
state_wc = () ->
  preload: ->
    game.load.image('bkg', 'img/plastic.gif')
    game.load.spritesheet('button', 'img/buttons.png', 32, 32)



    return
  create: ->
    back = game.add.sprite(0, 0, 'bkg')
    game.set_buttons()
    #Sobreescribir la funcion de los botones
    game.btn_a = game.add.button(350, 20, 'button', () ->
      alert "BTN_A"
    , 1, 0, 2)

    game.btn_b = game.add.button(350, 85, 'button', () ->
      alert "BTN_B"
    , 1, 0, 2)
    return
  update: ->

    return
  decisor: (s_function) ->

    return

# Light
## Apaga o enciende las luces
state_light = () ->
  preload: ->
    game.load.image('bkg', 'img/plastic.gif')
    game.load.spritesheet('button', 'img/buttons.png', 32, 32)



    return
  create: ->
    back = game.add.sprite(0, 0, 'bkg')
    game.set_buttons()
    #Sobreescribir la funcion de los botones
    game.btn_a = game.add.button(350, 20, 'button', () ->
      alert "BTN_A"
    , 1, 0, 2)

    game.btn_b = game.add.button(350, 85, 'button', () ->
      alert "BTN_B"
    , 1, 0, 2)
    return
  update: ->

    return
  decisor: (s_function) ->

    return

# medicine
## Cura al bicho cuando esta enfermo
state_medicine = () ->
  preload: ->
    game.load.image('bkg', 'img/plastic.gif')
    game.load.spritesheet('button', 'img/buttons.png', 32, 32)



    return
  create: ->
    back = game.add.sprite(0, 0, 'bkg')
    game.set_buttons()
    #Sobreescribir la funcion de los botones
    game.btn_a = game.add.button(350, 20, 'button', () ->
      alert "BTN_A"
    , 1, 0, 2)

    game.btn_b = game.add.button(350, 85, 'button', () ->
      alert "BTN_B"
    , 1, 0, 2)
    return
  update: ->

    return
  decisor: (s_function) ->

    return
