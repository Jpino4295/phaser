# Load all required libraries.
gulp = require 'gulp'
coffee = require 'gulp-coffee'


gulp.task('coffee', ->
  gulp.src('./coffee/*.coffee')
    .pipe(coffee({bare: true}).on('error', (err) ->
      console.log "ERR gulp: #{err}"
      ))
    .pipe(gulp.dest('./js'))
)


# # Default task call every tasks created so far.
gulp.task('default', ['watch'])

# Genera de forma temporal los archivos main.ect, ... dentro de views, que contiene una etiqueta script con attr inline
# Espera cambios sobre los scss/js
gulp.task 'watch', ->
  gulp.watch(['./coffee/*.coffee'], ['coffee'])



# Default task call every tasks created so far.
gulp.task('default', ['coffee', 'watch'])
