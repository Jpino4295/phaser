var game = new Phaser.Game(400, 200, Phaser.AUTO, 'phaser', this, true)


game.state.add("main", state.main_state)
game.state.add("hour", state.hour)

game.set_buttons = function(){
  game.add.sprite('button', './img/smile.png');

  game.button_a = game.add.button(350, 20, 'button', this.btn_a, 1, 0, 2);
  game.button_b = game.add.button(350, 85, 'button', this.btn_b, 1, 0, 2);
  game.button_c = game.add.button(350, 150, 'button', this.btn_c, 1, 0, 2);
}

game.btn_a = function() {
  //Poner todos los iconitos grises
  $(a_menu).css('opacity', '0.5')

  //Si el contador es mayor que 8, lo volvemos a poner a 0
  if(counter < 8){
    //Poner el icono que toca negro
    $("#"+a_menu[counter+1].id).css('opacity', '1')

  }
  // console.log(counter);
  counter += 1
},

game.btn_b = function() {
  var s_function = $("#"+a_menu[counter].id).attr('id');

  // game.state.start(s_function)
  game.state.getCurrentState()['decisor'](s_function)
},

//Cancelar(Aqui, resetear el menú)
game.btn_c = function() {
  game.state.start('main')
  counter = 0;
  $(a_menu).css('opacity', '0.5');
},


game.state.start('main')
