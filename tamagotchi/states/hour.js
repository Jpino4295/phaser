
state.hour = function(){}

var date = new Date()
var s_date = formatAMPM(date)

var o_style = { font: "65px clock", fill: "#000", align: "center" };
var o_style_ampm = {font: "65px clock", fill: "#000"}

state.hour.prototype = {
  preload: function(){
    this.set_buttons()


  },

  create: function(){
    var t = game.add.text(100, 0, s_date.s_hour, o_style)
    var t = game.add.text(110, 100, s_date.s_ampm.toUpperCase(), o_style_ampm)
  },

  update: function(){

  },

  decisor: function(){

  },

  set_buttons: function(){
    game.btn_a = function(){}
    game.btn_b = function(){}
    game.set_buttons()

  }

}

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var o_time = {s_hour: hours + ':' + minutes, s_ampm: ampm}
  return o_time;
}
