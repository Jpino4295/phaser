var demo = {}, centerX = 400/2, centerY = 400/2, bolita, speed = 150, pad, stick, speed, aux_dir, o_coords, counter, res_x, res_y ;

demo.state0 = function(){}

demo.state0.prototype = {
  preload: function(){
    // El metodo tiene: Nombre, path to sprite, dimension x, dimension y, n sprites
    // game.load.spritesheet('bolita_der', "assets/sprites/bolita_derecha_sheet_33x32.png", 33, 32, 8)
    // game.load.image('bolita', "assets/sprites/bolita.png")
    game.load.image('gatito', "assets/sprites/gatito.png")
    game.load.image('gatito_tumbado', "assets/sprites/gatito_tumbado.png")
    game.load.image('gatito_dormido', "assets/sprites/gatito_dormido.png")

    game.load.spritesheet('gatito_der', "assets/sprites/gatito_der_corriendo_sheet_32x32.png", 32, 32, 3)
    game.load.spritesheet('gatito_izq', "assets/sprites/gatito_izq_corriendo_sheet_32x32.png", 32, 32, 3)
    game.load.spritesheet('gatito_up', "assets/sprites/gatito_up_corriendo_sheet_32x32.png", 32, 32, 3)
    game.load.spritesheet('gatito_down', "assets/sprites/gatito_down_corriendo_sheet_32x32.png", 32, 32, 3)
    game.load.spritesheet('gatito_d_down_left', "assets/sprites/gatito_d_down_left_corriendo_sheet_32x32.png", 32, 32, 3)
    game.load.spritesheet('gatito_d_down_right', "assets/sprites/gatito_d_down_right_corriendo_sheet_32x32.png", 32, 32, 3)
    game.load.spritesheet('gatito_d_up_left', "assets/sprites/gatito_d_up_left_corriendo_sheet_32x32.png", 32, 32, 3)
    game.load.spritesheet('gatito_d_up_right', "assets/sprites/gatito_d_up_right_corriendo_sheet_32x32.png", 32, 32, 3)
    // game.load.spritesheet('bolita_izq', "assets/sprites/bolita_izquierda_sheet_33x32.png", 33, 32, 8)
    // game.load.image('bolita_salto', "assets/sprites/bolita_salto.png")

  },
  create: function(){
    counter = 0
    game.stage.backgroundColor = '#80ff80';
    // console.log("State 0");
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL

    //Fisicas para que no salga de la pantalla
    // game.physics.startSystem(Phaser.Physics.P2JS)
    game.physics.startSystem(Phaser.Physics.ARCADE)
    // Esta propiedad hace que rebote un poco en los laterales
    // game.physics.p2.defaultRestitution = 0.8

    game.physics.arcade.setBoundsToWorld()

    // console.log(centerX, centerY);
    // Instanciamos el sprite
    // bolita = game.add.sprite(centerX, centerY, 'bolita_der')
    gatito = game.add.sprite(centerX, centerY, 'gatito')

    init_anim()

    //Añadir evento si click en sprite
    // gatito.inputEnabled = true;
    // gatito.events.onInputDown.add(on_down, this);


    // Añadimos fisicas a nuestro sprite
    // game.physics.p2.enable(bolita)
    // game.physics.enable(bolita, Phaser.Physics.ARCADE);
    game.physics.enable(gatito, Phaser.Physics.ARCADE);


    // bolita.body.clearShapes()
    // bolita.body.loadPolygon("physicsData", 'bolita_der')

    //Determina si podemos chocar con las paredes o no. Default = true
    // bolita.body.collideWorldBounds = true;
    // //Determina si podemos rotamos al hacer una colision
    // bolita.body.fixedRotation = true;
    // // bolita.anchor.x = 0.5
    // // bolita.anchor.y = 0.5
    // bolita.anchor.setTo(0.5, 0.5)


    gatito.body.collideWorldBounds = true;
    gatito.body.fixedRotation = true;
    gatito.anchor.setTo(0.5, 0.5)

    // bolita.body.onBeginContact.add(wall_hit, this)

    // speed = game.rnd.integerInRange(400, 500)

    update_direction()

    //Definimos los cursores
    // cursors = game.input.keyboard.createCursorKeys();

  },

  update: function(){
    // set_movement()
    // console.log(o_coords);
    // console.log(parseInt(bolita.getBounds().x));
    // console.log(parseInt(bolita.getBounds().y));
    // console.log(o_coords);
    // if (bolita.getBounds().contains(o_coords.x, o_coords.y)){
    //   bolita.body.velocity.setTo(0, 0);
    //   // bolita.loadTexture("bolita")
    //   update_direction()
    // }

    if (gatito.getBounds().contains(o_coords.x, o_coords.y)){
      gatito.body.velocity.setTo(0, 0);
      // bolita.loadTexture("bolita")
      gatito.loadTexture("gatito")
      counter += 1
      update_direction()
    }
  }
}

function init_anim() {
  // //Añadir a animacion del spritesheet
  // bolita.animations.add('walk')
  // //Comenzar animacion(nombre, velocidad, repeticion)
  // bolita.animations.play('walk', 10, true)

  gatito.animations.add('walk')
  gatito.animations.play('walk', 10, true)
}

function update_direction(){

  // bolita.loadTexture("bolita")


  // bolita.animations.play('walk', 16, true)
  o_coords = {x: game.world.randomX, y: game.world.randomY}
  setTimeout(function(){


    // console.log(o_coords);
    // bolita_x = bolita.getBounds().x
    // bolita_y = bolita.getBounds().y
    determine_sprite()

    // game.physics.arcade.moveToXY(bolita, o_coords.x, o_coords.y, speed, 2000)


  }, 5000)

}

function determine_sprite() {
  console.log(counter);
  if (counter >= 5 && counter < 8) {
    sleep()
    console.log("COUNTER > 5");
  }else if (counter == 8){
    counter = 0
    update_direction()
  }else{
    res_x = this.o_coords.x - gatito.getBounds().x
    res_y = this.o_coords.y - gatito.getBounds().y

    if (res_x > 0 && res_y > 0) {
      console.log("Abajo derecha");
      gatito.loadTexture("gatito_d_down_right")
    } else if (res_x > 0 && res_y < 0) {
      console.log("arriba derecha");
      gatito.loadTexture("gatito_d_up_right")
    } else if (res_x < 0 && res_y > 0) {
      console.log("Abajo izquierda");
      gatito.loadTexture("gatito_d_down_left")
    } else if (res_x < 0 && res_y < 0) {
      console.log("arriba izquierda");
      gatito.loadTexture("gatito_d_up_left")
    } else if (res_x > 0 && res_y === 0) {
      console.log("izquierda");
      gatito.loadTexture("gatito_izq")
    } else if (res_x < 0 && res_y === 0) {
      console.log("derecha");
      gatito.loadTexture("gatito_der")
    } else if (res_x === 0 && res_y > 0) {
      console.log("Abajo");
      gatito.loadTexture("gatito_down")
    } else if (res_x === 0 && res_y < 0) {
      console.log("Arriba");
      gatito.loadTexture("gatito_up")
    }


    init_anim()
    game.physics.arcade.moveToXY(gatito, o_coords.x, o_coords.y, speed, 4500)
  }

}

function sleep(){


  setTimeout(function(){
    gatito.loadTexture("gatito_tumbado")
    setTimeout(function(){
      gatito.loadTexture("gatito_dormido")
      setTimeout(function(){
        counter += 1
        update_direction()
      }, 3000)
    }, 3000)
  }, 2000)
}
//
// function on_down () {
//   if (counter >= 1 ) {
//     update_direction()
//   }
// }
